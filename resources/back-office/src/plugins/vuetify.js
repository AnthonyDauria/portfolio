import Vue from 'vue';
import Vuetify from 'vuetify'
import VueRouter from 'vue-router'

Vue.use(Vuetify);
Vue.use(VueRouter);

const opts = {
    icons: {
        iconfont: 'mdiSvg',
    },
};

export default new Vuetify(opts);