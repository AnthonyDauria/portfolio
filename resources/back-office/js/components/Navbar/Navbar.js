import axios from 'axios';

export default {
    name: 'navBar',
    data: () => ({
        isLog: false
    }),
    mounted: async function() {
        const log = await axios.get(`${location.origin}/checkLogin/admin`);
        this.isLog = log.data.status;
        this.user = log.data.user;
    }
}