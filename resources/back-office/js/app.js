import Vue from 'vue';
import 'es6-promise/auto'
import Vuex from 'vuex';
import vuetify from '@plugins/vuetify';
require('./bootstrap');

Vue.use(Vuex);

Vue.component('navBar', require('@components/Navbar/Navbar.vue').default);
Vue.component('loginForm', require('@components/Login/Login.vue').default);

const app = new Vue({
    vuetify
}).$mount('#app');

window.App = app;