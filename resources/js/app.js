import Vue from 'vue';
import 'es6-promise/auto'
import Vuex from 'vuex';
import vuetify from '@plugins/vuetify';
require('./bootstrap');

Vue.use(Vuex);

Vue.component('navbar', require('@components/Header/Header.vue').default);
Vue.component('composantHome', require('@components/Home/Home.vue').default);
Vue.component('modalLogin', require('@components/Modal/Login/Login.vue').default);
Vue.component('modalRegister', require('@components/Modal/Register/Register.vue').default);
Vue.component('modalPassword', require('@components/Modal/Password/Password.vue').default);
Vue.component('composantFooter', require('@components/Footer/Footer.vue').default);
Vue.component('modalCode', require('@components/Modal/Code/Code.vue').default);
Vue.component('profils', require('@components/Profils/Profils.vue').default);
Vue.component('myProfil', require('@components/MyProfil/MyProfil.vue').default);
Vue.component('userProfil', require('@components/UserProfil/UserProfil.vue').default);

const app = new Vue({
    vuetify
}).$mount('#app');

window.App = app;