import axios from 'axios';

export default {
    name: "modalPassword",
    data: () => ({
        dialog: false,
        e6: 1,
        mail: '',
        code: '',
        rules: {
            mail : [
                val => val.length > 0 || 'Email requis',
                val => new RegExp(/^([A-Za-z0-9_\-.])+@([A-Za-z0-9_\-.])+\.([A-Za-z]{2,4})$/).test(val) || 'Votre email n\'est pas valide'
            ]
        },
    }),
    
    methods: {
        close: function() {
            this.dialog = false;
        },
        log: function() {
            this.dialog = false;
            this.$parent.$parent.$refs.modalLogin.dialog = true;
        },
        step2: function() {
            if (new RegExp(/^([A-Za-z0-9_\-.])+@([A-Za-z0-9_\-.])+\.([A-Za-z]{2,4})$/).test(this.mail)) {
                axios.get(`${location.origin}/account/resetPswd`, {
                    params: {
                        mail: this.mail
                    }
                }).then(function(data) {
                    console.log(data);
                    setToast(data);
                }).catch(function(data) {
                    console.log(data);
                });

                this.e6 = 2;
            }
        }
    },
}