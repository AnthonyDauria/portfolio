import { mdiClose } from '@mdi/js';
import axios from 'axios';

export default {
    name: "composantRegister",
    props: {
        show: Boolean
    },
    data: () => ({
        dialog: false,
        submit: false,
        showPswd: false,
        iconClose: mdiClose,
        valid: true,
        email: '',
        rules: {
            lastname: [
                val => val.length || 'Un nom est requis'
            ],
            firstname: [
                val => val.length || 'Un prénom est requis'
            ],
            mail: [
                val => val.length > 0 || 'Un email est requis'
            ],
            password: [
                val => new RegExp(/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[a-zA-Z]).{8,}$/).test(val) || 'Votre mot de passe doit contenir au moins 1 majuscule, 1 minuscule, 1 caractère spécial, 1 chiffre et doit contenir entre 8 et 24 caractères'
            ]
        }
    }),
    methods: {
        log: function() {
            this.dialog = false
            this.$parent.$parent.$refs.modalLogin.dialog = true;
        },
        close: function() {
            this.dialog = false;
        },
        register: function() {
            if (this.$refs.form.validate()) {
                const component = this;
                try {
                    axios.post(`${location.origin}/api/register`, $(this.$refs.form.$el).serialize())
                        .then(function(data) {
                            component.dialog = false;
                            if (data.data.step) {
                                component.$parent.$parent.$refs.modalCode.email = component.email;
                                component.$parent.$parent.$refs.modalCode.dialog = true;
                            } else {
                                component.$parent.$parent.$refs.modalLogin.dialog = true;
                            }
                        });
                } catch (error) {
                    console.log(error);
                }
            }
        }
    },
}