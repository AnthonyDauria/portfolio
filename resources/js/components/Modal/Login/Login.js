const axios = require('axios');

export default {
    name: 'modalLogin',
    props: {
        show: Boolean,
    },
    data: () => ({
        dialog: false,
        metatoken: "",
        showPswd: false,
        valid: true,
        submit: false,
        rules: {
            mail: [
                val => new RegExp(/^([A-Za-z0-9_\-.])+@([A-Za-z0-9_\-.])+\.([A-Za-z]{2,4})$/).test(val) || 'Votre email n\'est pas valide'
            ],
            pswd: [
                val => new RegExp(/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[a-zA-Z]).{8,}$/).test(val) || 'Votre mot de passe doit contenir au moins 1 majuscule, 1 minuscule, 1 caractère spécial, 1 chiffre et doit contenir entre 8 et 24 caractères'
            ]
        },
    }),
    mounted: function() {
        this.metatoken = $('meta[name="csrf-token"]').attr('content');
    },
    methods: {
        close: function() {
            this.dialog = false;
        },
        reg: function() {
            this.dialog = false;
            this.$parent.$parent.$refs.modalRegister.dialog = true;
        },
        resetPswd: function() {
            this.dialog = false;
            this.$parent.$parent.$refs.modalPswd.dialog = true;
        },
        login: async function() {
            if (this.$refs.form.validate()) {
                this.submit = !this.submit;
                const form = $(this.$refs.form.$el).serializeArray();
                let params = {};
                form.map(function(v) {
                    params[v.name] = v.value;
                });
                const login = await axios.get(`${location.origin}/login`, {
                    params: params
                });

                if (login.data.error) {
                    alert(login.data.error);
                } else {
                    if (login.data.modal) {
                        this.$parent.$parent.$refs.modalCode.dialog = true;
                        this.$parent.$parent.$refs.modalCode.email = $(this.$refs.form.$el).find('input[name="email"]').val();
                    } else {
                        this.$parent.$parent.log = true;
                        this.$store.state.user = login.user
                    }
                    this.dialog = false;
                }
                this.submit = false;
            }
        }
    }
}