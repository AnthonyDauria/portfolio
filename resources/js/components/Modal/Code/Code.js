export default {
    name: 'modalCode',
    data: () => ({
        email: '',
        dialog: false,
        valid: true,
        submit: false
    }),
    methods: {
        close: function() {
            this.dialog = false;
        },
        nextInput: function(el) {
            const target = $(el).data('target');
            if ($(el).val().length > 0) {
                $(`input[name="${target}"]`).focus();
            }
        },
        sendCode: function() {
            if (this.$refs.form.validate()) {
                this.submit = false;
                const component = this;
                axios
                    .post(`${location.origin}/api/account/validate`, {
                        mail: this.email,
                        code: $("input[name$='Code']").map(function() {
                            return this.value;
                        }).get().join("")
                    })
                    .then(function(data) {
                        component.submit = false;
                        if (data.data.type === 'error') {

                        } else {
                            component.dialog = false;
                        }
                    });
            }
        }
    }
}