import modalLogin from '@components/Modal/Login/Login.vue';
import modalRegister from '@components/Modal/Register/Register.vue';
import modalPassword from '@components/Modal/Password/Password.vue';
import modalCode from '@components/Modal/Code/Code.vue';

export default {
    name: "navbar",
    data: () => ({
        log: false,
        user: {}
    }),
    components: {
        modalLogin,
        modalRegister,
        modalPassword,
        modalCode
    },
    mounted: async function() {
        const log = await axios.get(`${location.origin}/checkLogin`);
        this.log = log.data.status;
        this.user = log.data.user;
    },
    methods: {
        goHome: () => {
            location.href = location.origin;
        },
        isLog: function() {
            return this.log;
        },
        logout: async function() {
            await axios.post(`${location.origin}/logout`);
            this.log = false;
            this.$store.state.user = {};
        },
        goTo: (src) => {
            location.href = `${location.origin}/${src}`
        }
    },
}