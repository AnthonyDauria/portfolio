import axios from "axios"

export default {
    name: "userProfil",
    props: {
        id_user: String
    },
    data: () => ({
        user: {}
    }),
    mounted: async function() {
        const user = await axios.get(`${location.origin}/api/profil/infoUser/${this.id_user}`);
        this.user = user.data.user;
    }
}