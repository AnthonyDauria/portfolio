export default {
    name: "Profils",
    data: () => ({
        profils: {},
    }),
    mounted: async function() {
        const profils = await axios.get(`${location.origin}/api/profils`);
        if (profils.data.profils.length > 0) {
            this.profils = profils.data.profils;
            this.profils.forEach($el => {
                $el.description = $el.description.substring(0, 50);
                const name = `${$el.user.lastname} ${$el.user.firstname}`;
                $el.user.slugname = name.toLowerCase().replace(/ /g, '-').replace(/[^\w-]+/g, '');
            });
        }


        $(document).ready(() => {
            const map = L.map("mapLeaflet", {
                center: [46.227638, 2.213749],
                zoom: 6
            });

            L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
                attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors',
            }).addTo(map);

            // L.marker([43.357340, 5.411119]).addTo(map);
        });

        $('#mapLeaflet').css('top',
            $('.headerNav').height()
        ).css('left',
            $('.col-sm-5').css('max-witdh')
        );

        $(window).on('resize', function() {
            $('#mapLeaflet').css('top',
                $('.headerNav').height()
            );
        });
    },
    methods: {
        accessProfil: function(slug) {
            location.href = `${location.origin}/profils/user/${slug}`;
        }
    }
};