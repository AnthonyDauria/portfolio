import axios from "axios";

export default {
    name: "myProfil",
    data: () => ({
        send: false,
        user: {}
    }),
    props: {
        iduser: String
    },
    mounted: async function() {
        const user = await axios.get(`${location.origin}/api/profil/user/${this.iduser}`);
        this.user = user.data.profil;
    },
    methods: {
        updateProfil: function() {
            this.send = true;
        }
    }
}