@extends('front.layout')

@section('meta')
    <meta name="description" content="Mon cv et mon portfolio">
    <link rel="canonical" href="/">
@endsection

@section('title')
    - Mon portfolio et mon cv en ligne
@endsection

@section('body')
    <composant-home></composant-home>
@endsection