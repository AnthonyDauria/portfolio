const mix = require('laravel-mix');
require('laravel-mix-alias');
const path = require('path');
require('vuetifyjs-mix-extension');
const webpack = require("webpack");
require('dotenv').config();
const CssnanoPlugin = require('cssnano-webpack-plugin');
const CompressionPlugin = require('compression-webpack-plugin');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel applications. By default, we are compiling the CSS
 | file for the application as well as bundling up all the JS files.
 |
*/

let js = 'js';
let css = 'css';
let src = 'src';
let sass = 'sass';

if (process.env.CURRENT_APP_NAME === "back-portfolio") {
    js = `back-office/${js}`;
    css = `back-office/${css}`;
    src = `back-office/${src}`;
    sass = `back-office/${sass}`;
}

mix.vuetify('vuetify-loader', {
    extract: `${css}/vuetify-components.css`
}).js(`resources/${js}/app.js`, `public/${js}/vue/main.js`).extract(['vue']).vue({
    version: 2,
    extractStyles: true,
    globalStyles: false
}).sass(`resources/${sass}/app.scss`, `public/${css}`);

mix.webpackConfig({
    mode: 'development',
    devServer: {
        inline: false,
    },
    optimization: {
        minimizer: [
            new CssnanoPlugin()
        ],
    },
    module: {
        rules: [{
                test: /\.scss$/,
                loader: "sass-loader",
            },
            {
                test: /\.css$/i,
                use: [
                    "style-loader",
                    "css-loader",
                    {
                        loader: "postcss-loader",
                        options: {
                            postcssOptions: {
                                plugins: [
                                    new webpack.DefinePlugin({
                                        'process.env.NODE_ENV': process.env.APP_KEY == 'local' ? 'development' : 'production'
                                    }), [
                                        "postcss-preset-env",
                                        {

                                        },
                                    ]
                                ],
                            },
                        },
                    },
                ],
            },
            {
                test: /\.(gif|png|jpe?g|svg)$/i,
                use: [{
                    loader: 'file-loader',
                    options: {
                        esModule: false,
                        name: '[path][name].[ext]',
                    }
                }],
            }
        ]
    },
    plugins: [
        new CompressionPlugin({
            filename: "[path][base].gz",
            algorithm: "gzip",
            test: /\.js$/,
            threshold: 10240,
            minRatio: 0.8,
            deleteOriginalAssets: 'keep-source-map',
        }),
        require('postcss-focus'),
        require('autoprefixer')
    ],
    resolve: {
        alias: {
            '@components': path.resolve(__dirname, `resources/${js}/components`),
            '@plugins': path.resolve(__dirname, `resources/${src}/plugins`),
            '@public': path.resolve(__dirname, 'public'),
        },
        extensions: ['.js', '.vue', '.json'],
        fallback: {
            fs: false,
            child_process: false
        }
    }
});

mix.version([`public/${js}/vue/main.js.gz`, `public/${js}/vue/vendor.js.gz`]);