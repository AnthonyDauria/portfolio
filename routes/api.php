<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('api')->group(function() {
    // Import des routes FrontOffice
    if (config('app.name') === "FrontOffice") {
        Route::post('/register', 'Front\AccountController@register');
        Route::get('/profils', "Front\ProfilsController@list");
        Route::get('/profil/user/{id}', 'Front\ProfilsController@getInfoProfil');
        Route::get('/profil/infoUser/{id}', "Front\ProfilsController@showProfilUser");
    }
    // Import des routes backOffice
    else {
        
    }
});
