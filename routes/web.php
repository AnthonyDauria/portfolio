<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


// Accès Front office
if (config('app.name') === "FrontOffice") {
    Route::get('/', 'Front\HomeController@index');

    // Account
    Route::get('account/resetPswd', 'Front\AccountController@resetPswd');
    Route::get('account/profil', 'Front\ProfilsController@viewProfil');

    // Acces sessions
    Route::get('/login', 'Front\AccountController@login');
    Route::post('/logout', 'Front\AccountController@logout');
    Route::post('/account/validate', 'Front\AccountController@valideAccount');

    Route::get('profils', 'Front\ProfilsController@all');
    Route::get('profils/user/{id}', 'Front\ProfilsController@userBySlug');
} 
// Accès Back-Office
else {
    Route::get('/', 'Admin\HomeController@index');
}

// Routes communes
Route::get('/checkLogin/{guard?}', 'Front\AccountController@checkLogin');