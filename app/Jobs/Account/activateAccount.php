<?php

namespace App\Jobs\Account;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class activateAccount implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    private $to;
    private $cc;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->to = "anthony.dauria@hotmail.fr";
        $this->cc = "anthony.dauria@hotmail.fr";
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        Mail::to($this->to)->cc($this->cc)->send(new activate($this->to));
    }
}
