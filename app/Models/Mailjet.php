<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use \Mailjet\Client;
use \Mailjet\Resources;

class Mailjet extends Model
{
    use HasFactory;

    private $key;
    private $secret;
    private $mj;

    public function __construct() {
        $this->key = config('app.mj_publikey');
        $this->secret = config('app.mj_privatekey');
        $this->mj = new Client($this->key, $this->secret, true, ['version' => 'v3.1']);
    }
    
    public function sendMail(string $template, array $variables, array $to, array $cc = [], array $bcc = []) : bool
    {
        $body = [
            'Messages' => [
                [
                    'From' => [
                        'Email' => "dauriaanthony13@gmail.com",
                        'Name' => "Mon Portfolio"
                    ],
                    'To' => [
                        $to
                    ],
                    'Cc' => [
                        $cc
                    ],
                    'Bcc' => [
                        $bcc
                    ],
                    'TemplateID' => $template,
                    'TemplateLanguage' => true,
                    'Variables' => $variables
                ]
            ]
        ];

        $response = $this->mj->post(Resources::$Email, ['body' => $body]);

        return $response->success();
    }

}
