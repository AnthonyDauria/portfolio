<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class SearchCity extends Model
{
    use HasFactory;

    protected $table = "search_city";

    public function user() : BelongsTo
    {
        return $this->belongsTo('App\Models\Users', 'id', 'id_user');
    }

    public function city() : BelongsTo
    {
        return $this->belongsTo('App\Models\City', 'id', 'id_city');
    }
}
