<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class City extends Model
{
    use HasFactory;

    protected $table = "city";

    public function searchcity() : HasMany
    {
        return $this->hasMany('App\Models\SearchCity', 'id_city');
    }
}
