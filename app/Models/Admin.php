<?php

namespace App\Models;

use App\Http\Middleware\Authenticate;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Admin extends Authenticate
{
    use HasFactory;

    protected $table = 'admin';

    protected $fillable = ["email"];
}
