<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\hasOne;
use Illuminate\Database\Eloquent\Relations\hasMany;

class Profils extends Model
{
    use HasFactory;

    protected $table = "profils";

    public function user() : BelongsTo {
        return $this->belongsTo('App\Models\User', 'id_user', 'id');
    }
}
