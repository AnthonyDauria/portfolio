<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\View\View;

class HomeController extends Controller
{
    public function index() : View
    {
        if (Auth::guard('admin')->check()) {
            return view('admin.home');
        } else {
            return view('admin.login');
        }
    }
}
