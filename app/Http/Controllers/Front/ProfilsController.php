<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use App\Models\Profils;
use App\Models\User;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;

class ProfilsController extends Controller
{
    public function all() {
        return view('front/profil/profils');
    }

    public function list() : Response {
        return response(["profils" => Profils::with('user')->get()] ,'200');
    }

    public function viewProfil() {
        return view('front/profil/showProfil', ['id_user' => Auth::id()]);
    }

    public function getInfoProfil(int $id) : Response {
        return response(["profil" => User::with(['myProfil:id_user,description,picture'])->select('id','civility','lastname','firstname','email','tel')->whereId($id)->first()], 200);
    }

    public function userBySlug(int $id) {
        return view('front.profil.user', ['id' => $id]);
    }

    public function showProfilUser(int $id) : Response {
        return response(["user" => User::with(["myProfil", "searchCity.city"])->first()], 200);
    }
}
