<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Exception;
use Illuminate\Support\Facades\Session;
use Laravel\Socialite\Facades\Socialite;

class AccountController extends Controller
{
    public function checkLogin(string $guard = '') : object {
        if ($guard) {
            return response([
                'status' => Auth::guard($guard)->check(),
                'user' => Auth::guard($guard)->user()
            ], "200");
        }
        else {
            return response([
                'status' => Auth::check(),
                'user' => Auth::user()
            ], "200");
        }
    }
    
    public function login (Request $r) : object {
        try {
            if ($user = $this->checkAccount($r->email, $r->password)) {

                if ($user->step === 1) {
                    return response(["modal" => "code", "200"]);
                }

                Auth::attempt($r->all());

                return response()->json(['success' => 'Successful log', 'user' => $user]);
            } else {
                throw new Exception("Vérifiez vos identifiants d'accès !");
            }
        }
        catch (Exception $ex) {
            return response()->json([
                'error' => (config('app.debug') == 'true' ? $ex->getMessage() : "Vérifiez vos identifiants d'accès !")
            ]);
        }
    }

    public function register (Request $r) : object {
        if (!$user = $this->checkAccount($r->mail, $r->pswd)) {
            $user = User::create([
                'civility' => $r->civility,
                'lastname' => $r->lastname,
                'firstname' => $r->firstname,
                'email' => $r->mail,
                'password' => bcrypt($r->pswd)
            ]);
        }

        switch ($user->step) {
            case 0 :
                // Étape d'envoie du mail avec le code de validation
                $user->code = $this->generateCode();
                $user->step++;
                $user->civility = $r->civility;
                $user->firstname = $r->firstname;
                $user->lastname = $r->lastname;
                $user->save();
                break;

            case 1 :
                // Étape d'accès au code, pas de code à executer
                break;

            default :
                return response()->json([
                    'type' => 'error',
                    'message' => 'Un compte avec cet email est déjà existant, veuillez vous connecter'
                ]);
        }

        return response()->json([
            'step' => $user->step
        ]);
    }

    public function logout () : object {
        try {
            if (!Auth::check()) {
                throw new Exception ('Vous devez d\'abord être connecté(e) !');
            }

            Auth::logout();
            return response()->json(['success' => 'Successful logout']);
        }
        catch (Exception $ex) {
            return response()->json([
                'error' => (config('app.debug') == 'true' ? $ex->getMessage() : 'Impossible de vous déconnecter pour le moment, veuillez réessayer plus tard !')
            ]);
        }
    }

    private function checkAccount(string $email, string $pswd) : ?User {
        $user = User::where([
            'email' => $email
        ])->first();

        if ($user && password_verify($pswd, $user->password)) {
            return $user;
        }
        
        return null;
    }

    public function resetPswd(Request $r) : object {
        if (!$user = User::where('mail', $r->mail)->first()) {
            return response()->json([
                'type' => 'error',
                'message' => 'Cette email n\'est pas présent dans la base'
            ]);
        }
        dd($user);

        $rs = ResetMail::firstOrCreate([
            'mail' => $r->mail
        ]);

        if (!$rs->code) {
            $rs->code = rand(0000, 9999);
            $rs->step = 2;
            $rs->save();
        }

        try {
            $this->mail->send($rs->mail, 'mail.code', $rs->code);
            return response()->json([
                'type' => 'success',
                'message' => "Mail envoyé"
            ]);
        } catch (Exception $ex) {
            return response()->json([
                'type' => 'error',
                'message' => $ex->getMessage()
            ]);
        }
    }

    public function resendMailPswd($mail) : object
    {
        $rs = ResetMail::where([
            'mail' => $mail
        ])->first();

        try {

        }
        catch (Exception $ex) {
            return response()->json([
                'type' => 'error',
                'message' => $ex->getMessage()
            ]);
        }
    }

    public function logFb() : object {
        return Socialite::driver('facebook')->redirect();
    }

    public function validLogFb() : object {
        $r = Socialite::driver('facebook')->user();

        $user = User::firstOrCreate([
            'token' => $r->getId(),
            'email' => $r->getEmail()
        ],
        [
            
        ]);

        return response()->json([
            'type' => 'success',
            'message' => 'Connexion réussit'
        ]);
    }

    public function logGoogle() : object {

    }

    public function logInsta() : object {

    }

    public function logTwitter() : object {

    }

    public function logApple() : object {
        
    }

    public function valideAccount(Request $r) : object {
        if (!$user = User::where(['email' => $r->mail, 'code' => $r->code])->first()) {
            return response()->json([
                'type' => 'error',
                'message' => 'Le code fourni ne correspond pas à votre mail, réessayez'
            ]);
        }

        $user->step = 2;
        $user->save();

        Auth::attempt(["email" => $user->email, "password" => $user->password]);
        
        return response()->json([
            "type" => 'success',
            'message' => 'Votre compte à bien été validé par notre équipe. Félicitation !'
        ]);
    }

    private function generateCode() : int {
        $arr = array();
        for ($i = 0; $i < 9999; $i++) {
            $arr[] = $i;
        }

        shuffle($arr);
        return array_rand($arr);
    }
}
