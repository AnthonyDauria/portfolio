<?php

namespace App\Http\Controllers\Front;

use Illuminate\Http\Request;
use Illuminate\View\View;
use App\Http\Controllers\Controller;

class HomeController extends Controller
{
    public function index () : View {
        return view('front/home');
    }
}
