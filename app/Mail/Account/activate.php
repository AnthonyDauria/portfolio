<?php

namespace App\Mail\Account;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Str;

class activate extends Mailable
{
    use Queueable, SerializesModels;
    private $to;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(string $to)
    {
        $this->to = $to;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $to = $this->to;
        $token = Str::random(40);
        return $this->view('email.activate', compact('to', 'token'));
    }
}
