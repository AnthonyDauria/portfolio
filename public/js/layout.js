// Appel de functions
$(document).ready(() => {
    setBodyHeight();
    $('.login').on('click', (e) => {
        e.preventDefault();
        $('.modalLogin').modal('show');
    });

    $('.switchPart').on('click', function() {
        const part = $(this).data('part');
        const currentPart = $(this).data('current');

        $(`.${currentPart}`).modal('hide');
        $(`.${part}`).modal('show');
    });

    $('.pswd').on('click', '.handlePassword', function() {
        const $btn = $(this);
        $btn.toggleClass('fa-eye').toggleClass('fa-eye-slash');
        if ($btn.hasClass('fa-eye')) {
            $btn.parent().find('.inputpswd').attr('type', 'password');
        } else {
            $btn.parent().find('.inputpswd').attr('type', 'text');
        }
    });

    $('.submitLogin').on('click', (e) => {
        e.preventDefault();
        $('.submitLogin .spinner-login').remove();

        $('.submitLogin').prepend(
            `<div class="spinner-login spinner-border text-light" role="status"></div>`
        );
    });

    $('.submitRegister').on('click', (e) => {
        e.preventDefault();
        $('.submitRegister .spinner-login').remove();

        $('.submitRegister').prepend(
            `<div class="spinner-login spinner-border text-light" role="status"></div>`
        );
    });

    $('.submitCode').on('click', (e) => {
        e.preventDefault();
        $('.submitCode .spinner-login').remove();

        $('.submitCode').prepend(
            `<div class="spinner-login spinner-border text-light" role="status"></div>`
        );
    });

    $('.formLog').on('submit', () => {
        $.ajax({
            url: `${location.origin}/login`,
            method: 'POST',
            data: {...$('.formLog').serializeArray() }
        }).then((data) => {
            setToast(data);
            if (data.success) {
                $('.navbar').load(' .navbar>*');
            }
        });
    });

    $('.logout').on('click', (e) => {
        e.preventDefault();
        $.ajax({
            url: `${location.origin}/logout`,
        }).then((data) => {
            setToast(data);
            if (data.success) {
                $('.navbar').load(' .navbar>*');
            }
        });
    });

    $('.checkAccount input').on('keyup', function(e) {
        const $input = $(this);
        const code = [97, 98, 99, 100, 101, 102, 103, 104, 105];
        if (code.includes(e.keyCode)) {
            switch ($input.attr('name')) {
                case 'first':
                    $('.checkAccount input[name="second"]').focus();
                    break;

                case 'second':
                    $('.checkAccount input[name="third"]').focus();
                    break;

                case 'third':
                    $('.checkAccount input[name="last"]').focus();
                    break;
            }
        }
    });

    window.fbAsyncInit = () => {
        FB.init({
            appId: '832812947360101',
            cookie: true,
            xfbml: true,
            version: 'v2.5'
        });
    };

    (function(d, s, id) {
        let js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) { return; }
        js = d.createElement(s);
        js.id = id;
        js.src = "https://connect.facebook.net/en_US/sdk.js";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));

    $('.btn-logFB').on('click', () => {
        FB.login(function(e) {
            if (e.status === 'connected') {
                let tk = e.authResponse.accessToken;
                $.ajax({
                    method: 'GET',
                    url: location.origin + '/login/facebook',
                    data: { token: tk },
                    success: function() {
                        location.href = "{{ url('/') }}"
                    }
                });
            }
        }, { scope: 'public_profile, email' });
    });
});

$(window).resize(() => {
    setBodyHeight()
});

// Définition des functions
const setBodyHeight = () => {
    $('body').css('height', (window.innerHeight));
}

const setToast = (data) => {
    if (data['type'] == 'error') {
        $('.error-toast').remove();
        $('.success-toast').remove();

        $('body').append(
            errorToast(data.error)
        );
    } else {
        $('.error-toast').remove();
        $('.success-toast').remove();

        $('body').append(
            successToast(data.success)
        );
    }

    $('.toast').css('position', 'absolute').css('right', '10px').css('bottom', $('.footer').innerHeight() + 10)
    $('.toast').toast('show');
}

// Définition des messages Toasts
const errorToast = (message) => {
    return `<div class='error-toast toast align-item-center text-white bg-danger border-0' role='alert' aria-live="assertive" aria-atomic="true">
                <div class='d-flex'>
                    <div class='toast-body'>
                        <i class="fas fa-exclamation-circle"></i>
                        <span class='errorMessage'>${message}</span>
                    </div>
                    <button type="button" class="btn-close btn-close-white me-2 m-auto" data-bs-dismiss="toast" aria-label="Close"></button>
                </div>
            </div>`;
}

const successToast = (message) => {
    return `<div class='success-toast toast align-item-center text-white bg-success border-0' role='alert' aria-live="assertive" aria-atomic="true">
                <div class='d-flex'>
                    <div class='toast-body'>
                        <i class="far fa-check-circle"></i>
                        <span class='successMessage'>${message}</span>
                    </div>
                    <button type="button" class="btn-close btn-close-white me-2 m-auto" data-bs-dismiss="toast" aria-label="Close"></button>
                </div>
            </div>`
}